package com.sixt.swipelist

import android.view.GestureDetector
import android.view.MotionEvent

interface OnUpAwareGestureListener : GestureDetector.OnGestureListener {

    fun onUp(motionEvent: MotionEvent)
}
