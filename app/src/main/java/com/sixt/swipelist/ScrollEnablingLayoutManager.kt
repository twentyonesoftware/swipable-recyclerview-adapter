package com.sixt.swipelist

import android.content.Context
import android.support.v7.widget.LinearLayoutManager

class ScrollEnablingLayoutManager(context: Context) : LinearLayoutManager(context) {
    private var isScrollEnabled = true

    fun setScrollEnabled(enabled: Boolean) {
        isScrollEnabled = enabled
    }

    override fun canScrollVertically(): Boolean {
        return isScrollEnabled
    }
}
