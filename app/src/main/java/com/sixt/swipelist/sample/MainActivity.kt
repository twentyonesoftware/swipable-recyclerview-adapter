package com.sixt.swipelist.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import com.sixt.swipelist.R
import com.sixt.swipelist.ScrollEnablingLayoutManager

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        val layoutManager = ScrollEnablingLayoutManager(this)
        val adapter = SampleAdapter(layoutManager)

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }
}
