package com.sixt.swipelist.sample

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.sixt.swipelist.R
import com.sixt.swipelist.ScrollEnablingLayoutManager
import com.sixt.swipelist.SwipeableRecyclerViewAdapter

class SampleAdapter(layoutManager: ScrollEnablingLayoutManager) : SwipeableRecyclerViewAdapter(layoutManager) {

    private val items = mutableListOf(
            "ITEM 1",
            "ITEM 2",
            "ITEM 3",
            "ITEM 4",
            "ITEM 5",
            "ITEM 6",
            "ITEM 7",
            "ITEM 8",
            "ITEM 9",
            "ITEM 10",
            "ITEM 11",
            "ITEM 12",
            "ITEM 13",
            "ITEM 14",
            "ITEM 15",
            "ITEM 16",
            "ITEM 17",
            "ITEM 18",
            "ITEM 19",
            "ITEM 20",
            "ITEM 21",
            "ITEM 22",
            "ITEM 23",
            "ITEM 24"
    )

    override fun getTopView(parent: ViewGroup): View =
            LayoutInflater.from(parent.context).inflate(R.layout.sample_top_layout, parent, false)

    override fun getBottomView(parent: ViewGroup): View =
            LayoutInflater.from(parent.context).inflate(R.layout.sample_bottom_layout, parent, false)

    override fun onBindViews(topView: View, bottomView: View, position: Int) {
        val topTextView = topView.findViewById<TextView>(R.id.topTextView)
        topTextView.text = items[position]

        val deleteTextView = bottomView.findViewById<TextView>(R.id.deleteTextView)
        deleteTextView.setOnClickListener {
            items.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, items.size)
        }

        val editTextView = bottomView.findViewById<TextView>(R.id.textView2)
        editTextView.setOnClickListener {
            Toast.makeText(bottomView.context, "Edit", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int = items.size
}
