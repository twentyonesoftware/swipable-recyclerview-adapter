package com.sixt.swipelist

interface SwipeListener {

    fun onBeforeSwipe(state: SwipeableRecyclerViewGestureListener.ItemState)
    fun onSwipe()
    fun onAfterSwipe(state: SwipeableRecyclerViewGestureListener.ItemState)
}
