package com.sixt.swipelist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class SwipeableRecyclerViewAdapter(val scrollEnablingLayoutManager: ScrollEnablingLayoutManager) : RecyclerView.Adapter<SwipeableRecyclerViewAdapter.ViewHolder>() {

    private var openItemGestureListener: SwipeableRecyclerViewGestureListener? = null

    abstract fun getTopView(parent: ViewGroup): View

    abstract fun getBottomView(parent: ViewGroup): View

    abstract fun onBindViews(topView: View, bottomView: View, position: Int)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.topLayout.translationX = 0.0F
        holder.topLayout.removeAllViews()
        holder.bottomLayout.removeAllViews()
        holder.topLayout.addView(getTopView(holder.topLayout))
        holder.bottomLayout.addView(getBottomView(holder.bottomLayout))
        onBindViews(holder.topLayout, holder.bottomLayout, position)

        val gestureListener = SwipeableRecyclerViewGestureListener(holder.topLayout, holder.bottomLayout)
        val gestureDetector = SwipeableRecyclerViewGestureDetector(holder.topLayout.context, gestureListener)

        gestureListener.setSwipeListener(object : SwipeListener {
            override fun onBeforeSwipe(state: SwipeableRecyclerViewGestureListener.ItemState) {
                if (state != SwipeableRecyclerViewGestureListener.ItemState.OPEN) {
                    openItemGestureListener?.scrollToStart()
                }
            }

            override fun onSwipe() {
                scrollEnablingLayoutManager.setScrollEnabled(false)
            }

            override fun onAfterSwipe(state: SwipeableRecyclerViewGestureListener.ItemState) {
                scrollEnablingLayoutManager.setScrollEnabled(true)
                if (state == SwipeableRecyclerViewGestureListener.ItemState.OPEN) {
                    openItemGestureListener = gestureListener
                } else {
                    openItemGestureListener = null
                }
            }
        })

        holder.topLayout.setOnTouchListener { _, motionEvent -> gestureDetector.onTouchEvent(motionEvent) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.swipable_adapter_view_holder, parent, false))

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val topLayout: ViewGroup = view.findViewById(R.id.topLayout)
        val bottomLayout: ViewGroup = view.findViewById(R.id.bottomLayout)
    }
}
