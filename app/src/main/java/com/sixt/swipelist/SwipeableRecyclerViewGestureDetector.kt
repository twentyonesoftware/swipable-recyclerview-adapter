package com.sixt.swipelist

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent

class SwipeableRecyclerViewGestureDetector(context: Context, private val gestureListener: OnUpAwareGestureListener) : GestureDetector(context, gestureListener) {

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_CANCEL) {
            gestureListener.onUp(ev)
        }
        return super.onTouchEvent(ev)
    }
}
