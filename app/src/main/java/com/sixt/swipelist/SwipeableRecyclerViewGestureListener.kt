package com.sixt.swipelist

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

class SwipeableRecyclerViewGestureListener(private val topView: View, private val bottomView: View) : GestureDetector.SimpleOnGestureListener(), OnUpAwareGestureListener {

    private var itemState: ItemState = ItemState.CLOSED
    private var topViewOffset = 0.0F
    private var bottomViewWidth = -1.0F
    private var swipeListener: SwipeListener? = null

    override fun onDown(e: MotionEvent?): Boolean {
        swipeListener?.onBeforeSwipe(itemState)
        if (bottomViewWidth < 0) {
            bottomViewWidth = bottomView.width.toFloat()
        }
        topViewOffset = topView.translationX
        return true
    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        topView.performClick()
        return super.onSingleTapUp(e)
    }

    override fun onUp(motionEvent: MotionEvent) {
        if (topView.translationX <= bottomViewWidth * -1) {
            scrollToBottomView()
        } else {
            scrollToStart()
        }
        swipeListener?.onAfterSwipe(itemState)
    }

    fun scrollToBottomView() {
        itemState = ItemState.OPEN
        topView.animate().translationX(bottomViewWidth * -1)
    }

    fun scrollToStart() {
        itemState = ItemState.CLOSED
        topView.animate().translationX(0.0F)
    }

    override fun onScroll(e1: MotionEvent, e2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
        val delta = e2.rawX - e1.rawX + topViewOffset
        if (topView.translationX <= 0) {
            swipeListener?.onSwipe()
            topView.translationX = delta
        }

        if (topView.translationX > 0) {
            topView.translationX = 0.0F
        }
        return super.onScroll(e1, e2, distanceX, distanceY)
    }

    fun setSwipeListener(listener: SwipeListener) {
        swipeListener = listener
    }

    enum class ItemState {
        OPEN, CLOSED
    }
}
